clc;
clear;
syms theta1 theta3 theta4 theta5 theta6;
% size of manipulator
l0=7;l1=9;l2=6.5;l3=5;tool_y=1.5;tool_x=9;
% Denavit-Hartenberg
a1=0;a2=0;a3=l1;a4=l2;a5=l3;a6=0;a7=0;a8=0;a9=tool_x;
apha1=0;apha2=pi/2;apha3=0;apha4=0;apha5=0;apha6=0;apha7=-pi/2;apha8=0;apha9=0;
d1=l0;d2=0;d3=0;d4=0;d5=0;d6=0;d7=0;d8=tool_y;d9=0;
% Computing Transform Matrixes
T1=sym('[cos(theta1) -sin(theta1)*cos(apha1) sin(theta1)*sin(apha1) a1*cos(theta1);sin(theta1) cos(theta1)*cos(apha1) -cos(theta1)*sin(apha1) a1*sin(theta1);0 sin(apha1) cos(apha1) d1;0 0 0 1]')
T2=sym('[cos(theta2) -sin(theta2)*cos(apha2) sin(theta2)*sin(apha2) a2*cos(theta2);sin(theta2) cos(theta2)*cos(apha2) -cos(theta2)*sin(apha2) a2*sin(theta2);0 sin(apha2) cos(apha2) d2;0 0 0 1]');
T3=sym('[cos(theta3) -sin(theta3)*cos(apha3) sin(theta3)*sin(apha3) a3*cos(theta3);sin(theta3) cos(theta3)*cos(apha3) -cos(theta3)*sin(apha3) a3*sin(theta3);0 sin(apha3) cos(apha3) d3;0 0 0 1]');
T4=sym('[cos(theta4) -sin(theta4)*cos(apha4) sin(theta4)*sin(apha4) a4*cos(theta4);sin(theta4) cos(theta4)*cos(apha4) -cos(theta4)*sin(apha4) a4*sin(theta4);0 sin(apha4) cos(apha4) d4;0 0 0 1]');
T5=sym('[cos(theta5) -sin(theta5)*cos(apha5) sin(theta5)*sin(apha5) a5*cos(theta5);sin(theta5) cos(theta5)*cos(apha5) -cos(theta5)*sin(apha5) a5*sin(theta5);0 sin(apha5) cos(apha5) d5;0 0 0 1]');
T6=sym('[cos(theta6) -sin(theta6)*cos(apha6) sin(theta6)*sin(apha6) a6*cos(theta6);sin(theta6) cos(theta6)*cos(apha6) -cos(theta6)*sin(apha6) a6*sin(theta6);0 sin(apha6) cos(apha6) d6;0 0 0 1]');
T7=sym('[cos(theta7) -sin(theta7)*cos(apha7) sin(theta7)*sin(apha7) a7*cos(theta7);sin(theta7) cos(theta7)*cos(apha7) -cos(theta7)*sin(apha7) a7*sin(theta7);0 sin(apha7) cos(apha7) d7;0 0 0 1]');
T8=sym('[cos(theta8) -sin(theta8)*cos(apha8) sin(theta8)*sin(apha8) a8*cos(theta8);sin(theta8) cos(theta8)*cos(apha8) -cos(theta8)*sin(apha8) a8*sin(theta8);0 sin(apha8) cos(apha8) d8;0 0 0 1]');
T9=sym('[cos(theta9) -sin(theta9)*cos(apha9) sin(theta9)*sin(apha9) a9*cos(theta9);sin(theta9) cos(theta9)*cos(apha9) -cos(theta9)*sin(apha9) a9*sin(theta9);0 sin(apha9) cos(apha9) d9;0 0 0 1]');
%T=T1*T2*T3*T4*T5*T6*T7*T8*T9;