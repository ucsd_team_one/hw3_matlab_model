function [x,y,z]=show_manipulator3D(theta1,theta3,theta4)
grid on;
axis equal
view(3);
% transfer theta
theta1=theta1*pi/180;
theta2=0;
theta3=theta3*pi/180;
theta4=theta4*pi/180;
% size of manipulator
l1=9;l2=6.5;l3=5;
% Denavit-Hartenberg
a1=0;a2=0;a3=l2;a4=l3;
apha1=0;apha2=pi/2;apha3=0;apha4=0;
d1=l1;d2=0;d3=0;d4=0;
% Computing Transform Matrixes
T1=[cos(theta1) -sin(theta1)*cos(apha1) sin(theta1)*sin(apha1) a1*cos(theta1);sin(theta1) cos(theta1)*cos(apha1) -cos(theta1)*sin(apha1) a1*sin(theta1);0 sin(apha1) cos(apha1) d1;0 0 0 1];
T2=[cos(theta2) -sin(theta2)*cos(apha2) sin(theta2)*sin(apha2) a2*cos(theta2);sin(theta2) cos(theta2)*cos(apha2) -cos(theta2)*sin(apha2) a2*sin(theta2);0 sin(apha2) cos(apha2) d2;0 0 0 1];
T3=[cos(theta3) -sin(theta3)*cos(apha3) sin(theta3)*sin(apha3) a3*cos(theta3);sin(theta3) cos(theta3)*cos(apha3) -cos(theta3)*sin(apha3) a3*sin(theta3);0 sin(apha3) cos(apha3) d3;0 0 0 1];
T4=[cos(theta4) -sin(theta4)*cos(apha4) sin(theta4)*sin(apha4) a4*cos(theta4);sin(theta4) cos(theta4)*cos(apha4) -cos(theta4)*sin(apha4) a4*sin(theta4);0 sin(apha4) cos(apha4) d4;0 0 0 1];
% show manipulator
x=0; y=0;z=0;
dx=T1(1,4);dy=T1(2,4);dz=T1(3,4);
line([x,dx],[y,dy],[z,dz],'linewidth',7);
x=dx; y=dy;z=dz;
T=T1*T2;
dx=T(1,4);dy=T(2,4);dz=T(3,4);
line([x,dx],[y,dy],[z,dz],'linewidth',5,'color','r');
x=dx; y=dy;z=dz;
T=T*T3;
dx=T(1,4);dy=T(2,4);dz=T(3,4);
line([x,dx],[y,dy],[z,dz],'linewidth',3,'color','g');
x=dx; y=dy;z=dz;
T=T*T4;
dx=T(1,4);dy=T(2,4);dz=T(3,4);
line([x,dx],[y,dy],[z,dz],'linewidth',2,'color','b');
x=dx;
y=dy;
z=dz;
